// console.log("Hello AL");


// Exponent Operator
let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
// Math.pow() allow us to get the result of namuiber raised  to the given exponent
// SYNTAX -> Math.pow(base,exponent)

// Exponent Operator - ** - allows us to get the result of a number raised to given exponenet
let  fivePowerOf2 = 5**2; 
console.log(fivePowerOf2); //25

// square root of a number with exponent operator
let squareRootOf4 = 4**.5;
console.log( squareRootOf4); //2

// Template Literals
// "",'' = formal name in JS isString Literals

let word1 ="JavaScript";
let word2 = "Java";
let word3 = "is";
let word4 = "not";


	// let sentence = (word1 +" "+ word2 +" "+ word3 + " not Java.");
	// console.log(sentence);

// ``  -> backticks -template literals
// ${} -> placeholder
let sentence1 = `${word1} ${word3} ${word4} ${word2}.`
console.log(sentence1);

let sentence2 = `${word2} is an OOP Language.`
console.log(sentence2);

let sentence3 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence3);
